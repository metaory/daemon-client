Daemon Client
=========

Nodejs Client daemon

  - work with rest api
  - store in mongo db


Requirement
--------------

```sh
install [NodeJs](http://nodejs.org/)
```


Installation
--------------

```sh
git clone https://metaory@bitbucket.org/metaory/daemon-client.git
cd daemon-client
npm install
node client

```



License
----

MIT


**Free Software, Hell Yeah!**

[Pou Yan]:metaory@gmail.com